console.log("Hi, B297!");

//Mini-Activity - log your favorite line 20 times in the console.

console.log("May the Force be with you.");


//Function Declaration
function printName() {
	console.log("My name is Jupiter.");
};

//Function Invocation
printName();

declaredFunction();

//Function declaration vs expressions

//Function Declaration
	//function declaration is created with the function keyword and adding a function name

//they are "saved for later use" and



function declaredFunction() {
	console.log("Hello from declaredFunction!");
};

declaredFunction();

//Function Expression
	//function expression is stored in a variable
	//function expression is an anonymous function assigned to the variable function

//variableFunction();

let variableFunction = function() {
	console.log("Hello from function expression!");
};

variableFunction();



let funcExpression = function funcName() {
	console.log("Hello from the other side!");
};

funcExpression();

declaredFunction = function() {
	console.log("updated declaredFunction");
};

declaredFunction();

funcExpression = function() {
	console.log("updated funcExpression");
};

funcExpression();

const constantFunc = function() {
	console.log("Initialized with const!");
};

constantFunc();


//Function Scoping

/*
	Score - accessibility/visibility of variables

	JS Variables has 3 types of score:
	1. local/block scope
	2. global scope
	3. function scope


*/

/*{
	let a = 1;
}

let a = 1;

function sample() {
	let a = 1;
}
*/

{
	let localVar = "Armando Perez";
}

let globalVar = "Mr. Worldwide";

//console.log(localVar);
console.log(globalVar);

function showNames () {
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

/*console.log(functionVar);
console.log(functionConst);
console.log(functionLet);*/

showNames();

//Nested Functions

function myNewFunction() {
	let name = "Jane";

	function nestedFunction() {
		let nestedName = "John";
		console.log(name);
	}

	//console.log(nestedName);

	nestedFunction();
}

myNewFunction();

//Function and Global Scope Variables

//Global Scoped Variable

let globalName = "Cardo";
	
function myNewFunction2() {
	let nameInside = "Hillary";
	console.log(globalName);
};

myNewFunction2();
//console.log(nameInside);

// Using alert()
//alert("This will run immediately when the page loads.")

function showSampleAlert() {
	alert("Hello, Earthlings! This is from a function!");
}

//showSampleAlert();
console.log("I will only log in the console when the alert is dismissed!");



// Using prompt()

//let samplePrompt = prompt("Enter your Name: ");

//console.log("Hi, I am " + samplePrompt);

//prompt returns an empty string when there is no input. or null if the user cancels the prompt()

function printWelcomeMessage() {
	let firstName = prompt("Enter your firstName: ");
	let lastName = prompt("Enter your last name: ");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
};

//printWelcomeMessage();


//The Return Statement
/*
	The return statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function
*/


function returnFullName() {
	//return "Jeffrey" + ' ' + "Smith" + ' ' + "Bezos";
	console.log("This message will not be printed!");
}

let fullName = returnFullName();
console.log(fullName);



function returnFullAddress() {
	let fullAddress = {
		street: "#44 Maharlika St.",
		city: "Cainta",
		province: "Rizal"
	};

	return fullAddress;

};

let myAddress = returnFullAddress();
console.log(myAddress);










