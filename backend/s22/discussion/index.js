console.log("Hello, B297!");

//Functions
	//Parameters and Arguments

/* function printName() {
	
		let nickname = prompt("Enter your nickname: ");
		console.log("Hi, " + nickname);

	}

	printName();*/

	function printName(name) {
	
		console.log("Hi, " + name);

	}

	printName("Cee");

	let sampleVariable = "Cardo";
	printName(sampleVariable);

	function checkDivisibilityBy8(num) {
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder)
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);
	checkDivisibilityBy8(9678);


	function checkDivisibilityBy4(num) {
		let remainder = num % 4;
		console.log("The remainder of " + num + " divided by 4 is: " + remainder)
		let isDivisibleBy4 = remainder === 0;
		console.log("Is " + num + " divisible by 4?");
		console.log(isDivisibleBy4);
	}

	checkDivisibilityBy4(56);
	checkDivisibilityBy4(95);
	checkDivisibilityBy4(444);



	function printFriends(friend1, friend2, friend3) {
		console.log("My name is Jupiter.");
		console.log("Friend 1: " + friend1);
		console.log("Friend 2: " + friend2);
		console.log("Friend 3: " + friend3);
		console.log("My three friends are: " + friend1 + ", " + friend2 + ", " + friend3 + ".");
	}

	printFriends("Pluto", "Mars", "Venus");


function returnFullName(firstName,middleName,lastName) {
	return firstName + ' ' + middleName + ' ' + lastName;
	console.log("This will not be printed!");
}

let completeName1 = returnFullName("Monkey","D","Luffy");
let completeName2 = returnFullName("Carlo","Tanggol","Dalisay");

console.log(completeName1 + " is my bestfriend!");
console.log(completeName2 + " is my friend!");











