

//[truthy and falsy]

//[conditional (ternary) operator]


let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);


switch (day) {
	case 'monday':
		console.log("The color of the day is blue!");
		break;

	case 'tuesday':
		console.log("The color of the day is yellow!");
		break;

	case 'wednesday':
		console.log("The color of the day is red!");
		break;

	case 'thursday':
		console.log("The color of the day is green!");
		break;

	case 'friday':
		console.log("The color of the day is brown!");
		break;
	case 'saturday':
		console.log("The color of the day is white!");
		break;
	case 'sunday':
		console.log("The color of the day is black!");
		break;
	default:
		console.log("Please input a valid day");
		break;
}

