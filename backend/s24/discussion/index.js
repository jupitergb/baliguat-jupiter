


console.log("Javascript Loops");


function greeting() {
    console.log("Hi, Jupiter!");
}

let countNum = 5;

while(countNum !== 0) {
    console.log("This is printed inside the sample loop:" + countNum);
    greeting();
    countNum--;
}

//number = Number (prompt("Give me a number"));

let myString = "Taylor Swift";
//characters in strings may be counted using the .length property
console.log(myString.length);

//accessing elements of a string
console.log(myString[0]); //T
console.log(myString[1]); //a
console.log(myString[2]); //y

//will create a loop that will print out individual letters of variable
for(let x = 0; x < myString.length; x++) {
    //console.log(myString[x]);
    console.log("Iteration: " + x + " ");
}


let myName = "Cardo";

for(let i = 0; i < myName.length; i++) {
    
    if(myName[i].toLowerCase() == 'a') {
        console.log(3);
    } else {
        console.log(myName[i]);
    }
}

let name = "Bernardo";

for (i = 0; i < name.length; i++) {
    
    if (name[i].toLowerCase() == 'a') {
        console.log("Continue to the next iteration");
        continue;
    } 
    console.log(name[i]);
    if(name[i].toLowerCase() == 'd') {              
        break;
    }
    
}
