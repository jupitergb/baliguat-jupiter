console.log("JS Objects!");

//[Objects]
/*
    - An object is a data type that is used to represent real world objects
    - Information stored in objects are represented in a "key:value" pair

*/
//Creating objects using object initializer/literal notation
/*
    Syntax
    let objectName = {
        keyA: valueA,
        keyA: valueB
    }
*/

    let ninja = {
        name: "Naruto",
        village: "Konoha",
        occupation: "Hokage"
    }

    console.log("Result from creating objects using initiazers/literal notation");
    console.log(ninja);
    console.log(typeof ninja);

    let dog = {
        name: "Whitey",
        color: "white"
    }

    //Creating objects using a constructor function

        /*
            Create a reusable function to create several objects that have the same data structure
            Instance - is a concrete occurence of any object which emphasizes on its distinct/unique identity of it
        */

        /*
            function ObjectName(keyA, keyB) {
                this.keyA = keyA;
                this.keyB = keyB;
            }
        */

        function Laptop(name, manufactureDate) {
            this.name = name;
            this.manufactureDate = manufactureDate;
        }

        /*
            "this" keyword allows us to assign a new object's properties by associating them with the values received from a constructor function's parameter 
        */

        /*
            "new" operator creates an INSTANCE of an object
            Object and instances are often interchanged because object literals (let object = {}) and instances (let object = new Object) are distinct/unique objects
        */

        //Instances
        let laptop1 = new Laptop('Lenovo', 2022);
        console.log('Result from creating objects using object constructor');
        console.log(laptop1);

        let myLaptop = new Laptop('MacBook Air', 2020);
        console.log('Result from creating objects using object constructor');
        console.log(myLaptop);

        /*
            invoke/call "Laptop" function instead of creating a new object instance returns undefined without the "new" operator because the "Laptop" function does not have a return statement
        */

        let oldLaptop = Laptop('Portal R2E CCMC', 1980);
        console.log("result from creating instance without new keyword");
        console.log(oldLaptop); //undefined

        //Min Activity 1 (5 min)
        //create 3 more instances of our Laptop consturctor
        //log these 3 instances in the console

        let myLaptop1 = new Laptop('MacBook Air 1', 2010);
        console.log('Result from creating objects using object constructor');
        console.log(myLaptop1);

        let myLaptop2 = new Laptop('MacBook Air 2', 2020);
        console.log('Result from creating objects using object constructor');
        console.log(myLaptop2);

        let myLaptop3 = new Laptop('MacBook Air 3', 2030);
        console.log('Result from creating objects using object constructor');
        console.log(myLaptop3);


        //Create empty objects
        let computer = {};
        let myComputer = new Object();
        console.log(computer);//{}
        console.log(myComputer);//{}

//[Access Object Properties]

        //1. dot notation
        console.log('Result of dot notation: ' + myLaptop.name);
        console.log('Result of dot notation: ' + myLaptop.manufactureDate);
        //2. square bracket notation
        console.log('Result of square bracket notation: ' + myLaptop['name']);
        console.log('Result of square bracket notation: ' + myLaptop['manufactureDate']);
        
//Access array objects
/* 
        Accessing array elements can also be done using square brackets
        Accessing object properties using the square bracket notation, and array indexes can cause confusion
*/

        let array = [laptop1, myLaptop];

        //square bracket
            //may be confused for accessing array indexes
        console.log(array[0]['name']);


            //differentiation between accessing arrays and object properties
            //this tells us that array[0] is an object by using the dot notation
    
        //dot notation
        console.log(array[0].name);

//[Initialize, Add, Delete, & Reassign Object Properties]

let car = {};

car.name = 'Honda Civic';
console.log('Result from adding properties using dot notation:')
console.log(car);

car['manufacture date'] = 2019;
console.log(car['manufacture date']);
// console.log(car.manufacture date);
console.log(car);
console.log(car.manufactureDate);//undefined

//delete object properties
 delete car['manufacture date'];
 console.log('Result from deleting properties:');
 console.log(car);

 //reassign object properties
 car.name = 'Dodge Charger R/T';
 console.log('Result from reassigning properies:');
 console.log(car);

 //[Object Methods]
 /* 
        A method is a function which is a propery of an object
        Methods are functions related to a specific object.
        Similiar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
 */

        let person = {
            name: "Cardo",
            talk: function() {
                console.log('Hello, my name is ' + this.name);
            }
        }

        console.log(person);
        console.log('Result of object methods:');
        person.talk();

        //walk
        person.walk = function() {
            console.log(this.name + ' walked a 25 steps forward!');
        }
        person.walk();

        let friend = {
            firstName: 'Nami',
            lastName: 'Misko',
            address: {
                city: 'Tokyo',
                country: 'Japan'
            },
            emails: ['name@sea.com', 'namimisko@gmail.com'],
            introduce: function() {
                console.log('Hello! my name is ' + this.firstName + ' ' + this.lastName);
            }
        }
        friend.introduce();

//[Real World Application of Objects!]

    /* 
            Scenario:
            1. We would like to create a game that would have several pokemon interact with each other
            2. Every pokemon would have the same set of states, propertes and functions
    */
//Use object literals

let myPokemon = {
    name: "Pikachu",
    level: 3,
    health: 100,
    attack: 50,
    tackle: function(){
        console.log("This Pokemon tackled Target Pokemon!");
        console.log("Target Pokemon's health is now reduced to Target Pokemon Health");
    },
    faint: function() {
        console.log("Pokemon fainted");
    }
}
console.log(myPokemon);
myPokemon.faint();

//create an object constructor

function Pokemon(name, level) {
    //Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + " tackled " + target.name);
        console.log(target.name + "'s health is now reduced to _targetPokemonHealth_");
    }
    this.faint = function(){
        console.log(this.name + ' fainted.');
    }
}

let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);
rattata.faint();