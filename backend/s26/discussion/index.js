console.log("Array Traversal!");

//Array are used to store multiple related data/values in a single variable
//for us to declare and create arrays, we use [] also known as "array literals"

let task1 = "Bruth Teeth";
let task2 = "Eat Breakfast";

let hobbies = ["Play League", "Read a book", "Listen to music", "Code"];
console.log(typeof hobbies);
//Arrays are actually a special type of object
//Does it have key value pairs? Yes. index number : element
//Arrays make it easy to manage, manipulate as set of data
//Method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
/* 
    Syntax:

    let/const arrayName = [elementA, elementB, elementC ...]
*/

//Common examples of arrays
let grades = [98.5, 94.6, 90.8, 88.9];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'HP', 'NEO', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

let mixedArr = [12, 'Asus', null, undefined, {}];
let arraySample = ['Cardo', 'One Punch Man', 25000, false];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);
console.log(arraySample);

/* 
    Mini-Activity

    1. Create a variable that will store an array of at least 5 of your daily routine in the weekends
    2. Create a variable which will store at least 4 capital cities in the world
    3. Log the variables in the console and send an ss in our hangouts (4 mins.)
*/
let tasks = ['eat', 'sleep', 'exercise', 'code', 'movies'];
let capitalCities = [
    'Tokyo',
    'London', 
    'Paris', 
    'Cairo'
];

console.log(tasks);
console.log(capitalCities);

//Aternative way to write arrays
let myTasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake react'
];

//Create an array with values from variables:
let username1 = 'gdragon22';
let username2 = 'gdino21';
let username3 = 'ladiesman217';
let username4 = 'transformers45';
let username5 = 'noobmaster68';
let username6 = 'gbutiki78';

let guildMembers = [username1, username2, username3, username4, username5, username6];
console.log(guildMembers);
console.log(myTasks);

//[.length property]

//allows us to get and set the total number of items or elements in an array
//the .length property of an array gives a number data type

console.log(myTasks.length);
console.log(capitalCities.length);

let blankArr = [];
console.log(blankArr.length);//0

//length property can also be used with strings

let fullName = "Cardo Dalisay";
console.log(fullName.length);

//length property can also set the total number of items in an array meaning we can actually delete the last item in the array or shorten the array by simply updating the length property of an array

myTasks.length = myTasks.length - 1;
console.log(myTasks.length);
console.log(myTasks);
delete myTasks[0];

//to delete a specific item in an array, we can employ array methods

//another example using decrementation
capitalCities.length--;
console.log(capitalCities);

//can we do the same with a string? No.
fullName.length = fullName.length - 1;
console.log(fullName.length);//13

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);//last element is empty

theBeatles[4] = fullName;
console.log(theBeatles);//5

theBeatles[theBeatles.length] = "Tanggol";
console.log(theBeatles);

//[Read from Arrays]
/* 
    Access elements with the use of their index
    Syntax:
    arrayName[index]
*/

console.log(capitalCities[0]);//access the first element of the array

console.log(grades[100]);//undefined

let lakersLegends = ["Kobe", "Shaq", "LeBron", "Magic", "Kareem"];
console.log(lakersLegends[2]);// 3rd element
console.log(lakersLegends[4]);// 5th element
//you can also save array items in another variable
let currentLaker = lakersLegends[2];
console.log(currentLaker);

console.log("Array before assignment");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log("Array after re-assignment");
console.log(lakersLegends);

let favoriteFoods = [
    "Tonkatsu",
    "Adobo",
    "Pizza",
    "Lasagna",
    "Sinigang"
];

/* 
    Mini activity #2
    update/reassign the last two items in the array with your own personal faves
    log the favoriteFoods array in the console with its UPDATED elements using their index number
    send an ss in our hangouts
*/

console.log(favoriteFoods);
favoriteFoods[3] = "Spaghetti";
favoriteFoods[4] = "Lechon";
console.log(favoriteFoods);

/* 
    Mini activity #3
    Create a function named findBlackMamba which is able to receive an index number as a single argument and return the item accessed by its index
        -function should be able to receive one argument
        -return the blackMamba accessed by the index
        -create a variable outside the function called blackMamba and store the value returned by the function in it
        -log the blackMamba variable (Kobe) in the console
*/



function findBlackMamba(index) {
    return lakersLegends[index];
}

let blackMamba = findBlackMamba(0);
console.log(blackMamba);

/* 
    Mini activity #4
    -Create a function called addTrainers that can receive a single argument
    -add the argument in the end of theTrainers array
    -invoke and add an argument to be passed in the function
    -log theTrainer's array in the console
    -send a screenshot of your console in the Batch Hangouts

    Trainers to be added:   Brock
                            Misty
*/

let theTrainers = ["Ash"];

function addTrainers(trainer) {
    theTrainers[theTrainers.length] = trainer;
}

addTrainers("Misty");
addTrainers("Brock");
console.log(theTrainers);

//Access the last element of an array

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
let lastElementIndex = bullsLegends.length-1;

console.log(bullsLegends[lastElementIndex]);
console.log(bullsLegends[bullsLegends.length-1]);

//Add items into the array

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);

newArr[newArr.length-1] = "Aerith Gainsborough";
console.log(newArr);

/* 
newArr.pop();
newArr.push();
newArr.shift();
newArr.unshift();
newArr.splice();
*/

//[Loop over an array]

for (let index = 0; index < newArr.length; index++) {
    console.log(newArr[index]);
}

let numArr = [5, 12, 30, 48, 40];

for(let index = 0; index < numArr.length; index++) {
    if(numArr[index] % 5 === 0) {
        console.log(numArr[index] + " is divisible by 5!")
    } else {
        console.log(numArr[index] + " is not divisible by 5!");
    }
}

//[Multidimensional] Arrays

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);

//access elements of a multidimensional array
console.log(chessBoard[1][4]);
console.log("Pawn moves to: " + chessBoard[1][5])//f2