console.log("Array Non-Mutator and Iterator Methods");


let countries = ['US','PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];


//indexOf
let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf method: ' + invalidCountry);

//lastIndexOf
let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method:' + lastIndex);

let lastindexStart = countries.lastIndexOf('PH', 6);
console.log('Result of lastIndexOf method:' + lastindexStart);

//slice
console.log(countries);

let slicedArrayA = countries.slice(2);
console.log('Result from slice method:')
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2,4);
console.log('Result from slice method:')
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log('Result from slice method:')
console.log(slicedArrayC);

//toString()
let stringArray = countries.toString();
console.log('Result from toString method: ');
console.log(stringArray);

let sampArr = [1,2,3];
console.log(sampArr.toString());

//concat()
let taskArrayA = ['drink html', 'eat javascript'];
let taskArrayB = ['inhale css', 'breathe mongodb'];
let taskArrayC = ['get git', 'be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method: ");
console.log(tasks);

console.log("Result from concat method: ");
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

let combinedTasks = taskArrayA.concat("smell express", "throw react");
console.log("Result from concat method: ");
console.log(combinedTasks);

//join
let users = ['John', 'Jane', 'Joe', 'James'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));

//Iteration Methods

//forEach()
console.log(allTasks);

allTasks.forEach(function(task) {
    console.log(task);
})

let filteredTasks = [];

allTasks.forEach(function(task) {

    if(task.length>10) {
        filteredTasks.push(task);
    }
});

console.log("Result of filteredTasks: ");
console.log(filteredTasks);

//map()

let numbers = [1,2,3,4,5];

let numberMap = numbers.map(function(number) {
    return number * number;
})

console.log('Original Array: ');
console.log(numbers);
console.log('Result of map method:')
console.log(numberMap);

//map() vs forEach()

let numberForEach = numbers.forEach(function(number) {
    return number * number;
})

console.log(numberForEach);

//every()
let allValid = numbers.every(function(number) {
    return (number < 3);
})

console.log("Resul of every method: ");
console.log(allValid);

//some()
let someValid = numbers.some(function(number) {
    return (number < 2);
})

console.log("Resul of some method: ");
console.log(someValid);

//filter()
let filterValid = numbers.filter(function(number) {
    return (number < 3);
})

console.log("Resul of filter method: ");
console.log(filterValid);

let nothingFound = numbers.filter(function(number) {
    return (number = 0);
})

console.log("Resul of filter method: ");
console.log(nothingFound);

//includes()
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

let filteredProducts = products.filter(function(product) {
    return product.toLowerCase().includes('a');
})

console.log(filteredProducts);

//reduce()
console.log(numbers);

let iteration = 0;
let reducedArray = numbers.reduce(function(x,y) {
    console.warn('Current iteration: ' + ++iteration);
    console.log('Accumulator: ' + x);
    console.log('Current value: ' + y);
    return x + y;
})
console.log("Result of reduce method: " + reducedArray);

//reduce string arrays to string
let list = ['Hello', 'From', 'The', 'Other', 'Side'];
let reducedJoin = list.reduce(function(x,y) {
    console.log(x);
    console.log(y);
    return x + ' ' + y;
})
console.log("Result of reduce method: " + reducedJoin); 




