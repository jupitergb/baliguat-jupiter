console.log("Hi from index.js");

//fetch() method has one argument by default, the url
//the url is a representative address of accessing a resource/data in another machine
//for now, we will use jsonplaceholder url, which is a sample server where we can get data from
//the .then() method will allow us to process the data we retrieved using fetch in another function
//the response is simply parameter. However, as a convention, indicating that we are now going to process the response from our server
//It is a representation of what we "got" from the server
//response.json() is a method to convert the incoming data as a proper JS object we can further process

//we can add another.then() method to do something with the processed server response

fetch("https://jsonplaceholder.typicode.com/posts").then(response=>response.json()).then(data=>{
    //console.log(data);
    //this will allow us to trigger the showPosts() function after we fetch the data from our server
    showPosts(data);
})
//Receive the feteched data as an argument
const showPosts = (posts) => {

    console.log(posts);
    
    //add each post as a string
    let postEntries = '';

    posts.forEach((post)=> {
        postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="deletePost('${post.id}')">Delete</button>            
            </div>        
        `
    })
    //console.log(postEntries);

    document.querySelector('#div-post-entries').innerHTML = postEntries;
}

//by default the page is refreshed upon form submission event, to prevent this, we can pass the event parameter to our function and use the preventDefault() method
document.querySelector('#form-add-post').addEventListener('submit',(event)=>{
    event.preventDefault();

    let titleInput = document.querySelector("#txt-title");
    let bodyInput = document.querySelector("#txt-body");

   /*  console.log(titleInput.value);
    console.log(bodyInput.value);
    console.log("Hello! The form has been submitted!"); */

    //whenever we try to add, update, and delete data to a server, we have to pass another argument to the fetch() method that contains other details
    //fetch("<URL>", {options})
        //options object should look like this:
    //method: this property tells the server what we intend to do, the value passed here are what we call the HTTP Methods 
        //GET(retrieve/read): For getting data in a server
        //POST(add): For add data in a server, We will use POST this time because want to ADD data to our server
        //PUT(update): For updating data in a server
        //DELETE(delete): for deleting data in a server

        //body: This property contains the main content that we want to send to our servers
        //headers: This property contains other details that we need to send to our server
    fetch("https://jsonplaceholder.typicode.com/posts",{
        method: 'POST',
        body: JSON.stringify({
            title: titleInput.value,
            body: bodyInput.value,
            userID: 1
        }),
        headers: {'Content-type':'application/json'}
    })
    .then((response)=>response.json())
    .then((data)=> {
        console.log(data);
        alert('Successfully added!');

        //clear the input elements after submission
        titleInput.value = null;
        bodyInput.value = null;
    })

    alert('Successfully added!');
})
