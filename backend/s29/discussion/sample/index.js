console.log('DOM Manipulation!');

console.log(document);
console.log(document.querySelector('#clicker')); //button element

/* 
    document - refers to the whole webpage
    querySelector - used to select a specific element (obj) as long as it is inside the html tag (html element)
    - takes a string that is formatted like CSS selector
    - can select elements regardless if the string is an id, class, or tag as tlong as the element is existing in the webpage
*/

/* 
    Alternative methods that we use aside from querySelector in retrieving elements:
        document.getElementByID()
        document.getElementByClassName()
        document.getElementByTagName()
*/

let counter = 0;

const clicker = document.querySelector('#clicker');
let value = 0;

    clicker.addEventListener('click',(event)=>{
        console.log("The button has been clcked.");
        counter++;
        alert(`The button has been clicked ${counter} times`);
    })

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");


const updateFullName = (e) => {
    let firstName = txtFirstName.value;
    let lastName = txtLastName.value;

    spanFullName.innerHTML = `${firstName} ${lastName}`
}

txtLastName.addEventListener('keyup', updateFullName);
txtFirstName.addEventListener('keyup', updateFullName);


/* txtFirstName.addEventListener('keyup',(event)=>{
    spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
})

const labelFirstName = document.querySelector("#label-txt-first-name");

labelFirstName.addEventListener('mouseover',(e)=>{
    alert("You hovered the First Name Label!");
}) */

/* 
    Mini-Activity
    Make a keyup event where the values of the first name and last name will populate the span element with the id #span-full-name
    Send the output in the batch google chat MA thread
*/

/* txtLastName.addEventListener('keyup',(event)=>{
    spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
})
 */