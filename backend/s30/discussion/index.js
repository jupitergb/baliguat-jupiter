console.log("ES6 Updates");

//ES6 Updates
    //ES6 is one of the latest versions of writing JS and in fact ONE of the MAJOR updates

    //let and const
        //are ES6 updates, these are the new standards of creating variables

    //In JS, hoisting allows us to use functions and variables before they are declared
    //BUT this might cause confusion, because of the confusion that var hoisting can create, it is BEST to AVOID using Variables before they are declared

    console.log(varSample);
    var varSample = "Hoist me up!";

    var nameVar = "Camille";

    if(true) {
        var nameVar = "Hi!";
    }

    var nameVar = "C";


    console.log(nameVar);

    let name1 = "Cee";

    if(true){
        let name1= "Hello";
    }

    console.log(name1);

    //[** Exponent Operator]

        //Update
        const firstNum = 8**2;
        console.log(firstNum);

        const secondNum = Math.pow(8, 2);
        console.log(secondNum);

    let string1 = 'fun';
    let string2 = 'Bootcamp';
    let string3 = 'Coding';
    let string4 = 'JavaScript';
    let string5 = 'Zuitt';
    let string6 = 'love';
    let string7 = 'Learning';
    let string8 = 'I';
    let string9 = 'is';
    let string10 = 'in';

    /* 
        MA1
        1. Create a new variable called concatSentence1
        2. Concatenate and save a resulting string into concatSentence1
        3. Log the concatSentence1 in your console and take a screenshot
        **the sentences MUST have spaces and punctuation
    */

        concatSentence1 = `${string1} ${string2} ${string3} ${string4} ${string5} ${string6} ${string7} ${string8} ${string9} ${string10}`;
        

        concatSentence1 = string1.concat(string2);

        console.log(concatSentence1);

        function sum(a,b) {
            return a+b;
        }

        const anotherMessage = `${sum(5,2)} attended a math competition.
        He wont it        
        `

        console.log(anotherMessage);
let name = "Peter";
        let dev = {
            name: "Peter",
            lastName: "Parker",
            occupation: "Web developer",
            income: 50000,
            expenses: 60000
        }

        console.log(`${dev.name} is a ${dev.occupation}`);

//[Array Destructuring]
/* 
    Allow us to unpack elements in arrays into distinct variables
    ALlows us to name array elements with variables instead of using index numbers
    Helps with code readability
*/

const fullName = ["Juan","Dela","Cruz"];
//Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

let [firstName, middleName, lastName] = fullName;
firstName = "Jupiter";
console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello ${firstName} ${middleName} ${lastName}! I was enchanted to meet you!`);
console.log(fullName);

let kupunanNiEugene = ["Eugene", "Alfred", "Vincent", "Dennis", "Taguro", "Master Jeremiah"];

const person = {
    givenName: "Jane",
    maidenName: "Dela",
    familyName: "Cruz",
}

const {givenName, maidenName,familyName} = person;

console.log(givenName);


person.givenName = "Jupiter";
console.log(person)
console.log(givenName);
console.log(person.givenName);

//[Arrow functions]
/* 
    Compact alternative syntax to traditional functions
    Useful for code snippets where creating functions will not be reused in any other portion of the code
    //Adhere to "DRY" (Don't Repeat Yourself) principles where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
*/

let displayHello = () => {
    
}

displayHello();

let printFullName = (firstName,middleInitial,lastName) => {
    console.log(`${firstName} ${middleInitial} ${lastName}`)
}

printFullName("John", "D", "Smith");

const students = ["John", "Jane", "Natalia", "Jobert", "Joe"];

//arrow functions with loops

//pre-arrow
students.forEach(function(student) {
    console.warn(`${student} is a studnet!`);
})

//arrow
students.forEach((student) => console.log(`${student} is a studnet!`))


//[Default Function Argument Value]

const greet = (name = "Jupiter") => {
    return `Good morning, ${name}!`
}

console.log(greet());
console.log(greet("John"));

class Car {
    constructor(brand,name,year) {
        this.brand = brand;
        this.name = name;
        this.year = year;
    }

}

let myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);

//Traditional functions vs Arrow function as methods

let character1 = {
    name: "Cloud Strife",
    occupation: "Soldier",
    greet: () => {
        console.log(this);
        console.log(`Hi! I'm ${this.name}`);
    },
    introduceJob: function() {
        console.log(`Hi! I'm ${this.name}. I'm a ${this.occupation}`);
    }
}

character1.greet()
character1.introduceJob()


/* 
    MiniA (3-5)
    create a "Character" class constructor
    name:
    role:
    strength:
    weakness:

    create 1 new instance of your character from the class constructor and save it in their respective variables
    -log the variables and send an ss
*/


class Character {
    constructor(name,role,strength,weakness) {
        this.name = name;
        this.role = role;
        this.strength = strength;
        this.weakness = weakness;
        this.introduce = () => {
            console.log(`Hi! i am ${this.name}`)
        }
    }
}

let char1 = new Character("Pluto","Developer","Analytical","Design");
console.log(char1);
char1.introduce();
