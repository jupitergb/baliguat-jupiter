let http = require("http");
let port = 4000;

let app = http.createServer((request,response)=>{
    /* 
        HTTP requests are differentiated not only via their endpoints but also with their METHODS
        HTTP methods simply tells the server what action it must take or what kind of response is needed for our request
        With an HTTP method, we can actually create routes with the same endpoint but with different methods
    */

    /* 
        url:localhost:4000/
        method: GET

        url:localhost:4000/
        method: POST
    */
    if(request.url == "/items" && request.method == "GET") {
        response.writeHead(200, {'Content-type':'text/plain'});
        response.end('Data received from the database!');
    }

    if(request.url == "/items" && request.method == "POST") {
        response.writeHead(200, {'Content-type':'text/plain'});
        response.end('Data to be sent to the database!');
    }
		


})

app.listen(port,()=>console.log(`Server running at localhost:${port}`));

