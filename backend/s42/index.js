//Setup a basix Express JS server

//Dependencies
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute");

//Server setup
const app = express();
const port = 4000;

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Database connection (Connect to MongoDB)
mongoose.connect("mongodb+srv://baliguatjupiter:iamKira96@batch-297.vvkeu5v.mongodb.net/taskDB?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error",console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database."))

app.use("/tasks", taskRoute);
//http://localhost:4000/tasks




//Server listening
if(require.main === module) {
    app.listen(port,()=>console.log(`Server running at port ${port}`));
}
module.exports = app;