//controller
const Course = require("../models/Course");



module.exports.addCourse = (req, res) =>{

	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	return newCourse.save().then((course,error)=>{
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}

	})
	.catch(err=>err)
}

//Retrieve all courses
/* 
	1. Retrieve all the courses from the database
*/

//We will use the find() method for our course model

module.exports.getAllCourses = (req, res) => {
	return Course.find({}).then(result=>{
		return res.send(result);
	})
}

module.exports.getAllActiveCourses = (req, res) => {
	return Course.find({isActive: true}).then(result=>{
		return res.send(result);
	})
}

module.exports.getCourse = (req, res) => {
	return Course.findById(req.params.courseId).then(result=>{
		return res.send(result);
	})
}

module.exports.updateCourse = (req,res)=>{
	let updatedCourse = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course, error)=>{
		if(error) {
			return res.send(false);
		} else {
			return res.send(true);
		}
	})
}

module.exports.archiveCourse = (req,res)=>{
	let archivedCourse = {
		isActive: false
	}

	return Course.findByIdAndUpdate(req.params.courseId, archivedCourse).then((course, error)=>{
		if(error) {
			return res.send(false);
		} else {
			return res.send(true);
		}
	})
}

module.exports.activateCourse = (req,res)=>{
	let activatedCourse = {
		isActive: true
	}

	return Course.findByIdAndUpdate(req.params.courseId, activatedCourse).then((course, error)=>{
		if(error) {
			return res.send(false);
		} else {
			return res.send(true);
		}
	})
}
