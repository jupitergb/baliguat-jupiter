//Route

//Dependencies and Modules

const express = require("express");
const courseController = require("../controllers/course");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;


//Routing Component
const router = express.Router();

//Routes - POST

//Creating a course if User is an Admin
router.post("/", verify, verifyAdmin, courseController.addCourse);

router.get("/all", courseController.getAllCourses);

router.get("/", courseController.getAllActiveCourses);

//get a specific couse
router.get("/:courseId", courseController.getCourse);

//edit a specific couse
router.put("/:courseId",verify, verifyAdmin, courseController.updateCourse);

//archive a specific couse
router.put("/:courseId/archive",verify, verifyAdmin, courseController.archiveCourse);

//activate a specific couse
router.put("/:courseId/activate",verify, verifyAdmin, courseController.activateCourse);



//Export Route System
module.exports = router;