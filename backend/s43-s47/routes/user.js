//Route

//Dependencies and Modules

const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;


//Routing Component
const router = express.Router();

//Routes - POST
//Check email
router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController))
})

//Register a user
router.post("/register",userController.registerUser)

//User Authentication

router.post("/login",userController.loginUser);
/* 
router.post("/details",(req, res)=>{
	userController.getProfile(req.body).then(resultFromController=>res.send(resultFromController))
}); */

router.post("/details", verify, userController.getProfile);

// Route to enroll user to a course
router.post("/enroll", verify, userController.enroll);

//Export Route System
module.exports = router;